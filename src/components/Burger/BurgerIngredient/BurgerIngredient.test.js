import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { configure, shallow } from "enzyme";
import BurgerIngredient from "./BurgerIngredient";

configure({ adapter: new Adapter() });

describe("<BurgerIngredient/>", () => {
  it("should render nothing with unrecognized type", () => {
    const wrapper = shallow(<BurgerIngredient type={"foo"} />);
    expect(wrapper.find(".Foo")).toHaveLength(0);
  });

  it("should render bread-top", () => {
    const wrapper = shallow(<BurgerIngredient type={"bread-top"} />);
    expect(wrapper.find(".BreadTop")).toHaveLength(1);
  });

  it("should render bread-bottom", () => {
    const wrapper = shallow(<BurgerIngredient type={"bread-bottom"} />);
    expect(wrapper.find(".BreadBottom")).toHaveLength(1);
  });

  it("should render meat", () => {
    const wrapper = shallow(<BurgerIngredient type={"meat"} />);
    expect(wrapper.find(".Meat")).toHaveLength(1);
  });

  it("should render cheese", () => {
    const wrapper = shallow(<BurgerIngredient type={"cheese"} />);
    expect(wrapper.find(".Cheese")).toHaveLength(1);
  });
});
