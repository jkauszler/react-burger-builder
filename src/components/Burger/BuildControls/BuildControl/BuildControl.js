import React from "react";
import styled from "styled-components";

const Control = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 5px 0;
  label {
    padding: 10px;
    font-weight: bold;
    width: 80px;
  }
  button {
    display: block;
    font: inherit;
    padding: 5px;
    margin: 0 5px;
    width: 80px;
    border: 1px solid #aa6817;
    cursor: pointer;
    outline: none;
    &:disabled {
      background-color: #ac9980;
      border: 1px solid #7e7365;
      color: #ccc;
      cursor: default;
    }
    &:disabled:hover {
      background-color: #ac9980;
      color: #ccc;
      cursor: not-allowed;
    }
    &.less {
      background-color: #d39952;
      color: white;
      &:active {
        background-color: #daa972;
        color: white;
      }
    }
    &.more {
      background-color: #8f5e1e;
      color: white;
      &:active {
        background-color: #99703f;
        color: white;
      }
    }
  }
`;

const buildControl = props => {
  return (
    <Control>
      <label>{props.label}</label>
      <button
        className="less"
        onClick={props.removed}
        disabled={props.disabled}
      >
        Less
      </button>
      <button className="more" onClick={props.added}>
        More
      </button>
    </Control>
  );
};

export default buildControl;
