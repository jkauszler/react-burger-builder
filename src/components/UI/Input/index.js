import React from "react";
import styled from "styled-components";

const Input = styled.div`
  width: 100%;
  padding: 10px;
  box-sizing: border-box;
  label {
    font-weight: bold;
    display: block;
    margin-bottom: 8px;
  }
  .input-element {
    outline: none;
    border: 1px solid #ccc;
    background-color: #fff;
    font: inherit;
    padding: 6px 10px;
    display: block;
    width: 100%;
    box-sizing: border-box;
    &:focus {
      outline: none;
      background-color: #ccc;
    }
  }
  .invalid {
    border: 1px solid red;
    background-color: salmon;
  }
`;

const input = props => {
  let inputElement = null;
  const inputClasses = ["input-element"];
  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push("invalid");
  }

  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          onChange={props.changed}
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
        />
      );
      break;
    case "textarea":
      inputElement = (
        <textarea
          onChange={props.changed}
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
        />
      );
      break;
    case "select":
      inputElement = (
        <select
          onChange={props.changed}
          className={inputClasses.join(" ")}
          value={props.value}
        >
          {props.elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          onChange={props.changed}
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
        />
      );
  }

  return (
    <Input>
      <label>{props.label}</label>
      {inputElement}
    </Input>
  );
};

export default input;
