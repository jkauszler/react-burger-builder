import React from "react";
import styled from "styled-components";

const StyledBackdrop = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 1000;
  left: 0;
  top: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const backdrop = props => {
  return props.show ? <StyledBackdrop onClick={props.clicked} /> : null;
};

export default backdrop;
