import React from "react";
import burgerLogo from "../../../assets/images/burger-logo.png";
import styled from "styled-components";

const Logo = styled.div`
  background: #fff;
  padding: 8px;
  height: 100%;
  box-sizing: border-box;
  border-radius: 5px;
  img {
    height: 100%;
  }
`;

const logo = props => {
  return (
    <Logo>
      <img src={burgerLogo} alt="Burger Builder" />
    </Logo>
  );
};

export default logo;
