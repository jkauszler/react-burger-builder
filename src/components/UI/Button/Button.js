import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  background-color: transparent;
  border: none;
  color: white;
  outline: none;
  cursor: pointer;
  font: inherit;
  padding: 10px;
  margin: 10px;
  font-weight: bold;
  &:first-of-type {
    margin-left: 0;
    padding-left: 0;
  }
  &.Success {
    color: #5c9210;
  }
  &.Danger {
    color: #944317;
  }
  &:disabled {
    color: #ccc;
    cursor: not-allowed;
  }
`;

const button = props => {
  return (
    <StyledButton
      disabled={props.disabled}
      className={props.btnType}
      onClick={props.clicked}
    >
      {props.children}
    </StyledButton>
  );
};

export default button;
