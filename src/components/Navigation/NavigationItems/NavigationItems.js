import React from "react";
import NavigationItem from "./NavigationItem/NavigationItem";
import styled from "styled-components";

const NavItems = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  @media (min-width: 500px) {
    flex-direction: row;
  }
`;

const navigationItems = props => {
  return (
    <NavItems>
      <NavigationItem link="/">Burger Builder</NavigationItem>
      <NavigationItem link="/orders">Orders</NavigationItem>
    </NavItems>
  );
};

export default navigationItems;
