import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

const NavItem = styled.li`
  margin: 10px 0;
  box-sizing: border-box;
  display: block;
  width: 100%;
  a {
    color: #8f5c2c;
    text-decoration: none;
    width: 100%;
    box-sizing: border-box;
    display: block;
    &:hover,
    &:active,
    &.active {
      color: #40a4c8;
    }
  }
  @media (min-width: 500px) {
    margin: 0;
    display: flex;
    width: auto;
    height: 100%;
    align-items: center;
    a {
      color: #fff;
      height: 100%;
      width: auto;
      padding: 16px 10px;
      border-bottom: 4px solid transparent;
      &:hover,
      &:active,
      &.active {
        background-color: #8f5c2c;
        border-bottom: 4px solid #40a4c8;
        color: #fff;
      }
    }
  }
`;

const navigationItem = props => {
  return (
    <NavItem>
      <NavLink to={props.link} exact>
        {props.children}
      </NavLink>
    </NavItem>
  );
};

navigationItem.propTypes = {
  active: PropTypes.bool
};

export default navigationItem;
