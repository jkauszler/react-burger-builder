import React from "react";
import styled from "styled-components";
import Logo from "../../UI/Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
// import Button from "../../UI/Button/Button";

const Toolbar = styled.header`
  width: 100%;
  height: 56px;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #703b09;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
  box-sizing: border-box;
  z-index: 90;
  & nav {
    height: 100%;
  }
  .logo {
    height: 80%;
  }
  @media (max-width: 499px) {
    .desktopOnly {
      display: none;
    }
  }
  .menuToggle {
    width: 40px;
    height: 100%;
    display: flex;
    flex-flow: column;
    justify-content: space-around;
    align-items: center;
    padding: 10px 0;
    box-sizing: border-box;
    cursor: pointer;
    div {
      width: 90%;
      height: 3px;
      background-color: white;
    }
  }
`;
const toolbar = props => {
  return (
    <Toolbar>
      {/* This menu toggle can be performed better. Maybe its on component */}
      <div className="menuToggle" onClick={props.opener}>
        <div />
        <div />
        <div />
      </div>
      <div className="logo">
        <Logo />
      </div>
      <nav className="desktopOnly">
        <NavigationItems />
      </nav>
    </Toolbar>
  );
};

export default toolbar;
