import React from "react";
import Logo from "../../UI/Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import styled from "styled-components";
import Backdrop from "../../UI/Backdrop/Backdrop";
import Aux from "../../../hoc/Aux/Aux";

const StyledSideDrawer = styled.div`
  position: fixed;
  width: 280px;
  max-width: 70%;
  height: 100%;
  left: 0px;
  top: 0px;
  z-index: 2000;
  background-color: #fff;
  padding: 32px 16px;
  box-sizing: border-box;
  transition: transform 0.3s ease-out;

  @media (min-width: 500px) {
    display: none;
  }

  &.open {
    transform: translateX(0);
  }

  &.close {
    transform: translateX(-100%);
  }

  .logo {
    height: 11%;
    margin-bottom: 32px;
  }
`;

const sideDrawer = props => {
  let toggle = "close";
  if (props.open) {
    toggle = "open";
  }
  return (
    <Aux>
      <Backdrop show={props.open} clicked={props.closed} />
      <StyledSideDrawer className={toggle}>
        <div className="logo">
          <Logo />
        </div>
        <nav>
          <NavigationItems />
        </nav>
      </StyledSideDrawer>
    </Aux>
  );
};

export default sideDrawer;
