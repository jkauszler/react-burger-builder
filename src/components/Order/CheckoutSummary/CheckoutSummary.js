import React from "react";
import Burger from "../../Burger/Burger";
import Button from "../../UI/Button/Button";
import styled from "styled-components";

const StyledCheckoutSummary = styled.div`
  text-align: center;
  width: 100%;
  margin: auto;
  @media (min-width: 600px) {
    width: 500px;
  }
`;

const checkoutSummary = props => {
  return (
    <StyledCheckoutSummary>
      <h1>We Hope it tastes great!</h1>
      <div style={{ margin: "auto" }}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button clicked={props.checkoutCanceled} btnType="Danger">
        CANCEL
      </Button>
      <Button clicked={props.checkoutContinued} btnType="Success">
        CONTINUE
      </Button>
    </StyledCheckoutSummary>
  );
};

export default checkoutSummary;
