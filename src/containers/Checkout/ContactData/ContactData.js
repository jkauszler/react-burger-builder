import React, { Component } from "react";
import styled from "styled-components";
import Button from "../../../components/UI/Button/Button";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Input from "../../../components/UI/Input";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import axios from "../../../axios-orders";
import withErrorHandler from "../../../hoc/WithErrorHandler/WithErrorHandler";

const FormContainer = styled.div`
  margin: 20px auto;
  width: 80%;
  text-align: center;
  box-shadow: 0 2px 3px #ccc;
  border: 1px solid #eee;
  padding: 10px;
  box-sizing: border-box;

  @media (min-width: 600px) {
    width: 500px;
  }
`;

class ContactData extends Component {
  state = {
    orderForm: {
      name: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your Name"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      street: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your Street Address"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      city: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your City"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      state: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your State"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      zip: {
        elementType: "input",
        elementConfig: {
          type: "number",
          placeholder: "Your Zip"
        },
        value: "",
        validation: {
          required: true,
          minLength: 5,
          maxLength: 5
        },
        valid: false,
        touched: false
      },
      email: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your Email"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      deliveryOrPickup: {
        elementType: "select",
        elementConfig: {
          options: [
            { value: "pick-up", displayValue: "Pick Up" },
            { value: "delivery", displayValue: "Delivery" }
          ]
        },
        value: "pick-up",
        validation: {},
        valid: true,
        touched: false
      }
    },
    formIsValid: false
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }
    // if (rules.minLength) {
    //   isValid = value.length() >= rules.minLength && isValid;
    // }
    // if (rules.maxLength) {
    //   isValid = value.length() <= rules.maxLength && isValid;
    // }
    return isValid;
  }

  orderHandler = event => {
    event.preventDefault();
    const formData = {};
    for (let formElmId in this.state.orderForm) {
      formData[formElmId] = this.state.orderForm[formElmId].value;
    }
    const order = {
      ingredients: this.props.ings,
      // Technically you would want to calc this on the server rather
      // trusting the clients app.
      price: this.props.price.toFixed(2),
      orderData: formData
    };
    this.props.onPurchase(order);
  };

  inputChangeHandler = (event, inputID) => {
    // Need to deeply clone the object and not the objects referrence pointer
    const updatedForm = { ...this.state.orderForm };
    const updatedFormElm = { ...updatedForm[inputID] };
    updatedFormElm.value = event.target.value;
    updatedFormElm.valid = this.checkValidity(
      updatedFormElm.value,
      updatedFormElm.validation
    );
    updatedFormElm.touched = true;
    updatedForm[inputID] = updatedFormElm;

    let formValid = true;
    for (let inputID in updatedForm) {
      formValid = updatedForm[inputID].valid && formValid;
    }
    this.setState({ orderForm: updatedForm, formIsValid: formValid });
  };

  render() {
    // Handle rendering state before the return
    let formElementsArray = [];
    for (let key in this.state.orderForm) {
      formElementsArray.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }
    let form = (
      <form onSubmit={this.orderHandler}>
        {formElementsArray.map(elm => (
          <Input
            key={elm.id}
            elementType={elm.config.elementType}
            elementConfig={elm.config.elementConfig}
            value={elm.config.value}
            invalid={!elm.config.valid}
            shouldValidate={elm.config.validation}
            touched={elm.config.touched}
            changed={event => this.inputChangeHandler(event, elm.id)}
          />
        ))}
        <Button btnType="Success" disabled={!this.state.formIsValid}>
          ORDER
        </Button>
      </form>
    );
    if (this.props.loading) {
      form = <Spinner />;
    }
    return (
      <FormContainer>
        <h4>Enter Your Contact Information</h4>
        {form}
      </FormContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    loading: state.orders.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPurchase: orderData => dispatch(actions.purchaseBurgerAsync(orderData))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ContactData, axios));
