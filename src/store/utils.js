// This helper method assumes that both the old object and new properties
// are store as js object.
export const updateObject = (oldObject, newProps) => {
  return {
    ...oldObject,
    ...newProps
  };
};
